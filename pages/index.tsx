import { useState, useRef, useEffect } from "react";
import Webcam from "react-webcam";
import "@tensorflow/tfjs-core";
import "@tensorflow/tfjs-converter";
import "@tensorflow/tfjs-backend-webgl";
import * as faceLandmarksDetection from "@tensorflow-models/face-landmarks-detection";
import * as blazeface from "@tensorflow-models/blazeface";
import { MediaPipeFaceMesh } from "@tensorflow-models/face-landmarks-detection/dist/types";
import { generateFaceDetecFrame } from "./mask";
import Image from "next/image";

interface PhotoSacleInterface {
  width: number;
  height: number;
}

const App = () => {
  const webcam = useRef<Webcam>(null);
  const canvas = useRef<HTMLCanvasElement>(null);
  const [photo, setPhoto] = useState<string>("");
  const [isFaceDetect, setIsFaceDetect] = useState<boolean>(false);
  const [photoSacle, setPhotoSacle] = useState<PhotoSacleInterface>({
    width: 0,
    height: 0,
  });

  useEffect(() => {
    runFaceDetect();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [webcam.current?.video?.readyState]);

  const runFaceDetect = async () => {
    const faceDetectModel = await faceLandmarksDetection.load(
      faceLandmarksDetection.SupportedPackages.mediapipeFacemesh
    );

    const blazefaceModel = await blazeface.load();

    drawFaceFrame(faceDetectModel, blazefaceModel);
  };

  const drawFaceFrame = async (
    faceDetectModel: MediaPipeFaceMesh,
    blazefaceModel: blazeface.BlazeFaceModel
  ) => {
    if (webcam.current && canvas.current) {
      const webcamCurrent = webcam.current as any;
      if (webcamCurrent.video.readyState === 4) {
        const video = webcamCurrent.video;
        const videoWidth = webcamCurrent.video.videoWidth;
        const videoHeight = webcamCurrent.video.videoHeight;
        const ctx = canvas.current.getContext("2d") as CanvasRenderingContext2D;
        canvas.current.width = videoWidth;
        canvas.current.height = videoHeight;
        setPhotoSacle({
          width: videoWidth,
          height: videoHeight,
        });
        const predictionsFace = await faceDetectModel.estimateFaces({
          input: video,
        });
        const predictionBlazeFace = await blazefaceModel.estimateFaces(
          video,
          false
        );
        requestAnimationFrame(() => {
          generateFaceDetecFrame(
            ctx,
            predictionsFace,
            predictionBlazeFace,
            setIsFaceDetect
          );
        });

        drawFaceFrame(faceDetectModel, blazefaceModel);
      }
    }
  };

  const capture = () => {
    if (webcam.current) {
      const imageSrc = webcam.current.getScreenshot();
      if (imageSrc) {
        setPhoto(imageSrc);
      }
    }
  };

  return (
    <div className="App" style={{ position: "relative" }}>
      <button disabled={!isFaceDetect} onClick={capture}>
        CAPTURE
      </button>
      <>
        <Webcam
          audio={false}
          ref={webcam}
          style={{
            position: "absolute",
            margin: "auto",
            textAlign: "center",
            top: 100,
            left: 0,
            right: 0,
          }}
        />
        <canvas
          style={{
            position: "absolute",
            margin: "auto",
            textAlign: "center",
            top: 100,
            left: 0,
            right: 0,
            zIndex: 9,
            // background: 'rgba(0,0,0,0.5)',
          }}
          ref={canvas}
        />
      </>
      {photo !== "" ? (
        <Image
          src={photo}
          width={photoSacle.width}
          height={photoSacle.height}
          alt="photo"
        />
      ) : (
        <></>
      )}
    </div>
  );
};

export default App;
