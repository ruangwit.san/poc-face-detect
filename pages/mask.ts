import { MaskMapping } from '../src/constants/mask'
import { AnnotatedPrediction } from '@tensorflow-models/face-landmarks-detection/dist/mediapipe-facemesh'
import { forEach } from 'lodash'
import * as blazeface from '@tensorflow-models/blazeface'
import { Dispatch, SetStateAction } from 'react'

export const generateFaceDetecFrame = (
  canvasContext: CanvasRenderingContext2D,
  predictionsFace: AnnotatedPrediction[],
  predictionBlazeFace: blazeface.NormalizedFace[],
  callback: Dispatch<SetStateAction<boolean>>
): boolean => {
  let isDetect = false
  canvasContext.lineWidth = 4
  forEach(predictionsFace, (face) => {
    forEach(predictionBlazeFace, (blaze) => {
      isDetect = face.faceInViewConfidence === 1 && (blaze.probability as number) > 0.999
      canvasContext.strokeStyle = isDetect ? 'green' : 'black'
    })
    callback(isDetect)
    canvasContext.beginPath()
    canvasContext.save()
    canvasContext.moveTo(329.4278564453125, 84.79412841796875)
    MaskMapping.forEach((mask) => {
      canvasContext.lineTo(mask.top, mask.buttom)
    })
    canvasContext.closePath()
    canvasContext.stroke()
    canvasContext.restore()
  })
  return isDetect
}
